Guia Inbox
==========

Considerações de Backend
------------------------

Ficou definido que a funcionalidade de usuário presumido, em que o cliente não é "deslogado" do site mesmo após o fim da sessão, é um requisito para o funcionamento do Inbox. Do contrário, as mensagens só poderão ser vistas caso o cliente decida "logar".

O disparo de uma mensagem está associado a uma data, a um evento ou a uma combinação de ambos; e sua distribuição depende dos perfis dos clientes, por exemplo:

* Mensagem a ser enviada no dia 20, às 15h, para todos os clientes que compraram calças chino nos últimos 3 meses.
* Mensagem a ser enviada em qualquer data, para todos os clientes que colocarem R$ 500,00 ou mais em suas sacolas.

As mensagens têm uma vida útil padrão de cinco dias, parâmetro que pode ser alterado caso a caso. A vida útil de uma mensagem é contada a partir do momento em que o disparo efetivamente ocorre.

A ação do cliente de ler uma mensagem não a exclui de sua listagem imediatamente. É dado um período de 24h em que ela continuará aparecendo no Inbox, e só então será descartada. Caso a vida útil da mensagem termine antes das 24 horas extras, a mensagem será excluída no final da vida útil.

A ação do cliente de excluir uma mensagem do Inbox tem efeito imediato, independente de sua vida útil. Essa ação não pode ser desfeita.

Há um limite de cinco mensagens visíveis por Inbox. A chegada de uma nova mensagem exclui automaticmante a mais antiga. Essa ação não pode ser desfeita.

Mensagens com vida útil expirada não aparecem no Inbox de nenhum cliente, mas podem ser vistas no Backend. Deve-se manter um histórico no Backend para fins de SAC e relatórios. Deve ser possível ver quem recebeu uma determinada mensagem, quem leu e quando leu ou quem não leu.

Deve ser possível utilizar dados variáveis no texto da mensagem, como nome do cliente, por exemplo. Também deve ser possível formatar a mensagem com bold e itálico e incluir links de html.

Gostaríamos, para o Inbox e para outros meios de comunicação, que existisse uma maneira de passar um cupom de desconto diretamente via URL, ou seja, um cupom que fosse aplicado automaticamente ao ser clicado um link.

Frontend
--------

Imaginamos a funcionalidade como algo simples. As mensagens teriam um período de vida útil e então seriam descartadas. Não haveria uma tela específica de listagem de mensagens e nem a visualização de mensagens anteriores (já descartdas). Toda a ação ocorreria exclusivamente dentro do menu.

* O clique no envelope abre o menu, se estiver fechado.

![](http://www.richards.com.br/inbox/rch-inbox-01.png)

* O clique no envelope fecha o menu, se estiver aberto. O clique no “×” fecha o menu.

![](http://www.richards.com.br/inbox/rch-inbox-02.png)

* O clique no título abre a mensagem. Ao abrir a mensagem, ela é marcada como lida e o contador diminui.

![](http://www.richards.com.br/inbox/rch-inbox-03.png)

* O clique na seta volta para a listagem. A mensagem já lida não desaparece e pode ser lida novamente, mas perde o destaque.

![](http://www.richards.com.br/inbox/rch-inbox-04.png)

* O clique em 'excluir' remove a mensagem da listagem. A mensagem excluída não poderá mais ser lida e a ação não pode ser desfeita.

![](http://www.richards.com.br/inbox/rch-inbox-08.png)

* Quando todas as mensagens forem lidas, todas perdem o destaque e o contador desaparece.

* Se não há nenhuma mensagem no banco, um aviso é mostrado ao abrir o menu.

![](http://www.richards.com.br/inbox/rch-inbox-05.png)

* A mensagem é composta por título e conteúdo. O conteúdo pode conter formatação (bold e itálico) e links.

![](http://www.richards.com.br/inbox/rch-inbox-06.png)

* O clique no link leva para o destino; se for dentro do site, um cookie deve fazer com que a caixa de mensagens permaneça aberta no mesmo estado; se for fora do site, o link deve ser aberto numa nova aba.

![](http://www.richards.com.br/inbox/rch-inbox-07.png?abc)